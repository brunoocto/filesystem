<?php

namespace Brunoocto\Filesystem\Tests;

use org\bovigo\vfs\vfsStream;
use Orchestra\Testbench\TestCase as TestbenchCase;
use Brunoocto\Filesystem\Providers\FilesystemServiceProvider;
use Brunoocto\Exception\Providers\ExceptionServiceProvider;

/**
 * TestCase for Service Provider
 */
class TestCase extends TestbenchCase
{
    /**
     * Get package providers.
     * @param  Illuminate\Foundation\Application  $app
     * @return array
     */
    protected function getPackageProviders($app)
    {
        return [
            FilesystemServiceProvider::class,
            ExceptionServiceProvider::class,
        ];
    }

    /**
      * Initialize environment
      * @return void
      */
    protected function getEnvironmentSetup($app)
    {
        // Initialize Virtual disk
        $app->singleton('vfs_stream', function () {
            return vfsStream::setup('vfs');
        });

        // Set Virtual Public path
        $app->singleton('path.public', function () {
            vfsStream::newDirectory('public', 0750)->at(app('vfs_stream'));
            return vfsStream::url('vfs').'/public';
        });

        // Set Virtual Storage path
        vfsStream::newDirectory('storage', 0750)->at(app('vfs_stream'));
        $app->useStoragePath(vfsStream::url('vfs').'/storage');
    }
}
