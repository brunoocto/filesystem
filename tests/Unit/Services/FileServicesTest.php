<?php

namespace Brunoocto\Filesystem\Tests\Unit\Services;

use Brunoocto\Filesystem\Tests\TestCase;
use Brunoocto\Filesystem\Services\FileService;
use Brunoocto\Filesystem\Services\FolderService;
use Brunoocto\Filesystem\Contracts\FileInterface;

class FileServicesTest extends TestCase
{
    /**
     * Test File path setting
     *
     * @return void
     */
    public function testSetPathForFile()
    {
        // It should failed since the path does not exists yet
        $filesystem = new FileService();
        $check = $filesystem->setPath(storage_path().'/pathA/pathB/pathC/test.txt');
        $this->assertFalse($check);

        // It should work because we create the path first
        $folder = new FolderService();
        $folder->createPath(storage_path().'/pathA/pathB/pathC', 0755);
        $filesystem = new FileService();
        $check = $filesystem->setPath(storage_path().'/pathA/pathB/pathC/test.txt', true);
        $this->assertEquals(storage_path().'/pathA/pathB/pathC/test.txt', $filesystem->getPath());
        $this->assertTrue($check);
    }

    /**
     * Test File permissions
     *
     * @return void
     */
    public function testFilePermissions()
    {
        $filesystem = new FileService();

        // It should failed since the path is not set yet
        $check = $filesystem->setPermissions();
        $this->assertFalse($check);

        // It should work since the path exists
        $filesystem->setPath(storage_path().'/test.txt', true);
        $check = $filesystem->setPermissions();
        $this->assertTrue($check);
        $check = $filesystem->setPermissions(0600);
        $this->assertTrue($check);
    }

    /**
     * Test File symlink
     *
     * @return void
     */
    public function testFileSymlink()
    {
        $basename = 'filesystem_test_file_'.uniqid();
        // We must use real path because "symlink" does not work in VFS
        $real = '/tmp/'.$basename.'.real';
        $symlink = '/tmp/'.$basename.'.symlink';

        $filesystem = new FileService();

        // It should fail because the path is not set
        $filesystem->recursiveRemove($symlink);
        $check = $filesystem->createSymlink($symlink);
        $this->assertFalse($check);

        // It should work since the path exists
        $filesystem->setPath($real, true);
        // Symlink does not work within VFS, so we check in tmp
        $filesystem->recursiveRemove($symlink);
        $filesystem->createSymlink($symlink);
        $this->assertFileExists($symlink);
        
        $filesystem->recursiveRemove($symlink);
        $filesystem->recursiveRemove($real);
    }

    /**
     * Test to get the list of Folder content
     *
     * @return void
     */
    public function testFileZip()
    {
        $basename = 'filesystem_test_file_'.uniqid();
        // We must use real path because ZipArchive does not work in VFS
        $txt = '/tmp/'.$basename.'.txt';
        $zip = '/tmp/'.$basename.'.zip';
        
        $filesystem = new FileService($txt, true);
        // It should fail if we try to zip a non-existing file
        $filesystem->recursiveRemove($txt);
        $check = $filesystem->createZip($zip);
        $this->assertFalse($check);

        // Zip creation should work
        $filesystem = new FileService($txt, true);
        $filesystem->recursiveRemove($zip);
        $check = $filesystem->createZip($zip);
        $this->assertTrue($check);
        $this->assertFileExists($zip);
        
        // Check that the regeneration works
        $check = $filesystem->createZip($zip);
        $this->assertTrue($check);
        $this->assertFileExists($zip);

        $filesystem->recursiveRemove($txt);
        $filesystem->recursiveRemove($zip);
    }

    /**
     * Test File dependance injection
     *
     * @return void
     */
    public function testFileDependanceInjection()
    {
        $dependance_injection = app()->make(FileInterface::class);
        $this->assertInstanceOf(FileInterface::class, $dependance_injection);
    }

    /**
     * Test get latest file
     *
     * @return void
     */
    public function testFileGetLatest()
    {
        // Should use filemtime since the file exists
        $file_path = '/exists.txt';
        $filesystem = new FileService();
        $filesystem->setPath(public_path().$file_path, true);
        $file_latest = FileService::getLatestPublic($file_path);
        $this->assertMatchesRegularExpression('/'.preg_quote($file_path, '/').'\?\d+/', $file_latest);

        // Should use time because the file is missing
        $file_path = '/missing.txt';
        $file_latest = FileService::getLatestPublic($file_path);
        $this->assertMatchesRegularExpression('/'.preg_quote($file_path, '/').'\?\d+/', $file_latest);
    }

    /**
     * Test File creation
     *
     * @return void
     */
    public function testFileCreation()
    {
        // Doubling the creation of the same file should work
        $file_path = storage_path().'/double.txt';
        $filesystem = new FileService();
        $filesystem->createPath($file_path);
        $check = $filesystem->createPath($file_path, 0600); // Note that chmod is ignored in VFS mode
        $this->assertTrue($check);
        $check = file_exists($file_path);
        $this->assertTrue($check);

        // It should fail when we try to create a file in a folder that does not exist
        $file_path = storage_path().'/no_existing_path/check.txt';
        $filesystem = new FileService();
        $check = $filesystem->setPath($file_path, true);
        $this->assertFalse($check);
        $check = file_exists($file_path);
        $this->assertFalse($check);
    }
}
