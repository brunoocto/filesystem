<?php

namespace Brunoocto\Filesystem\Tests\Unit\Services;

use Brunoocto\Filesystem\Tests\TestCase;
use Brunoocto\Filesystem\Services\FileService;
use Brunoocto\Filesystem\Services\FolderService;
use Brunoocto\Filesystem\Contracts\FolderInterface;

class FolderServicesTest extends TestCase
{
    /**
     * Test Folder path setting
     *
     * @return void
     */
    public function testSetPathForFolder()
    {
        // It should fail because the path does not exists yet
        $filesystem = new FolderService();
        $check = $filesystem->setPath(resource_path().'/pathA/pathB/pathC');
        $this->assertFalse($check);

        // It should fail because the path dis relative
        $filesystem = new FolderService();
        // Start listening to expection
        $this->expectException(\Exception::class);
        $check = $filesystem->setPath('pathA/pathB/pathC');
        // Stop listening to expection
        $this->expectException(null);
        $this->assertFalse($check);

        // It should work since we force the path
        $filesystem = new FolderService();
        $filesystem->setPath(storage_path().'/pathA/pathB/pathC', true);
        $this->assertEquals(storage_path().'/pathA/pathB/pathC', $filesystem->getPath());

        // It should work since the path has been previously created
        $filesystem = new FolderService();
        $check = $filesystem->setPath(storage_path().'/pathA/pathB/pathC');
        $this->assertTrue($check);
    }

    /**
     * Test Folder permissions
     *
     * @return void
     */
    public function testFolderPermissions()
    {
        $filesystem = new FolderService();

        // It should failed since the path is not set yet
        $check = $filesystem->setPermissions();
        $this->assertFalse($check);

        // It should work since the path exists
        $filesystem->setPath(storage_path().'/pathA/pathB/pathC', true);
        $check = $filesystem->setPermissions();
        $this->assertTrue($check);
        $check = $filesystem->setPermissions(0755);
        $this->assertTrue($check);
    }

    /**
     * Test Folder symlink
     *
     * @return void
     */
    public function testFolderSymlink()
    {
        $basename = 'filesystem_test_folder_'.uniqid();
        // We must use real path because ZipArchive does not work in VFS
        $real = '/tmp/'.$basename.'/real';
        $symlink = '/tmp/'.$basename.'/symlink';

        // It should work since the path exists
        $filesystem = new FolderService();

        // It should fail because the path is not set
        $filesystem->recursiveRemove($symlink);
        $check = $filesystem->createSymlink($symlink);
        $this->assertFalse($check);

        // Symlink does not work within VFS, so we check in tmp
        $filesystem->setPath($real, true);
        $filesystem->recursiveRemove($symlink);
        $filesystem->createSymlink($symlink);
        $this->assertFileExists($symlink);

        $filesystem->recursiveRemove($symlink);
        $filesystem->recursiveRemove($real);
    }

    /**
     * Test to get the list of Folder content
     *
     * @return void
     */
    public function testFolderLoop()
    {
        $basename = 'filesystem_test_folder_'.uniqid();
        // We must use real path because "glob" and "symlink" does not work in VFS
        $path = '/tmp/'.$basename;

        // It should work since the path exists
        $filesystem = new FolderService();
        $filesystem->recursiveRemove($path);
        $filesystem->setPath($path.'/aaa', true);
        $filesystem->setPath($path.'/bbb', true);
        $filesystem->createSymlink($path.'/symlink');
        $filesystem->setPath($path.'/ccc/ddd', true);
        $filesystem = new FileService();
        $filesystem->setPath($path.'/fa.txt', true);
        $filesystem->setPath($path.'/fb.txt', true);
        $filesystem->createSymlink($path.'/symlink.txt');
        $filesystem->setPath($path.'/ccc/fc.txt', true);
        $filesystem->setPath($path.'/ccc/ddd/fd.txt', true);
        
        $filesystem = new FolderService($path);
        $loop_folder = $filesystem->loopFolder();
        
        $this->assertArrayHasKey('folder', $loop_folder);
        $this->assertArrayHasKey('aaa', $loop_folder['folder']);
        $this->assertEquals($loop_folder['folder']['aaa'], $path.'/aaa');

        $this->assertArrayHasKey('file', $loop_folder);
        $this->assertArrayHasKey('fa.txt', $loop_folder['file']);
        $this->assertEquals($loop_folder['file']['fa.txt'], $path.'/fa.txt');

        $this->assertArrayHasKey('folder_link', $loop_folder);
        $this->assertArrayHasKey('symlink', $loop_folder['folder_link']);
        $this->assertEquals($loop_folder['folder_link']['symlink'], $path.'/bbb');

        $this->assertArrayHasKey('file_link', $loop_folder);
        $this->assertArrayHasKey('symlink.txt', $loop_folder['file_link']);
        $this->assertEquals($loop_folder['file_link']['symlink.txt'], $path.'/fb.txt');
        
        $filesystem->recursiveRemove($path);
    }

    /**
     * Test to get the list of Folder content
     *
     * @return void
     */
    public function testFolderZip()
    {
        $basename = 'filesystem_test_folder_'.uniqid();
        // We must use real path because ZipArchive does not work in VFS
        $path = '/tmp/'.$basename;
        $zip = '/tmp/'.$basename.'.zip';

        $filesystem = new FolderService($path);

        // It should fail if we try to zip a non-existing file
        $filesystem->recursiveRemove();
        $check = $filesystem->createZip($zip);
        $this->assertFalse($check);

        // Preparation of complex folder
        $filesystem->setPath($path.'/aaa', true);
        $filesystem->setPath($path.'/bbb/ccc', true);
        $filesystem = new FileService();
        $filesystem->setPath($path.'/bbb/fa.txt', true);

        // Zip creation should work
        $filesystem = new FolderService($path, true);
        @unlink($zip);
        $check = $filesystem->createZip($zip);
        $this->assertTrue($check);
        $this->assertFileExists($zip);

        // Check that the regeneration works
        $check = $filesystem->createZip($zip);
        $this->assertTrue($check);
        $this->assertFileExists($zip);

        @unlink($zip);
        $filesystem->recursiveRemove($path);
    }

    /**
     * Test Folder dependance injection
     *
     * @return void
     */
    public function testFolderDependanceInjection()
    {
        $dependance_injection = app()->make(FolderInterface::class);
        $this->assertInstanceOf(FolderInterface::class, $dependance_injection);
    }

    /**
     * Test File creation
     *
     * @return void
     */
    public function testFolderCreation()
    {
        // Doubling the creation of the same folder should work
        $file_path = storage_path().'/double';
        $filesystem = new FolderService();
        $filesystem->createPath($file_path);
        $check = $filesystem->createPath($file_path, 0600); // Note that chmod is ignored in VFS mode
        $this->assertTrue($check);
        $check = file_exists($file_path);
        $this->assertTrue($check);

        // It should fail when we try to create a file in a folder that does not exist
        $file_path = storage_path().'///permissions';
        $filesystem = new FolderService();
        $check = $filesystem->createPath($file_path); // Note that chmod is ignored in VFS mode
        $this->assertFalse($check);
        $check = file_exists($file_path);
        $this->assertFalse($check);
    }
}
