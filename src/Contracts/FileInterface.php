<?php

namespace Brunoocto\Filesystem\Contracts;

use Brunoocto\Filesystem\Contracts\FilesystemInterface;

interface FileInterface extends FilesystemInterface
{
    public static function getLatestPublic($file_path);
}
