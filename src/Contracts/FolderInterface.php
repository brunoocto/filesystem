<?php

namespace Brunoocto\Filesystem\Contracts;

use Brunoocto\Filesystem\Contracts\FilesystemInterface;

interface FolderInterface extends FilesystemInterface
{
    public function loopFolder();
}
