<?php

namespace Brunoocto\Filesystem\Contracts;

interface FilesystemInterface
{
    public function isAbsolutePath(string $path);

    public function setPath($path, $create = false);

    public function getPath();

    public function setPermissions($chmod = self::CHMOD);

    public function createPath($path, $chmod = self::CHMOD);

    public function createSymlink($symlink);

    public function createZip($zip_path);

    public function recursiveRemove($path = false);
}
