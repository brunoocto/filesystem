<?php

namespace Brunoocto\Filesystem\Services;

use Brunoocto\Filesystem\Services\FolderService;
use Brunoocto\Filesystem\Contracts\FileInterface;
use Brunoocto\Filesystem\Abstracts\FilesystemAbstract;

/**
 * Handle File operations
 *
 */
class FileService extends FilesystemAbstract implements FileInterface
{
    /**
     * Format file name for cache browser
     *
     * @param string $file_path relative file path from public folder only
     * @return string File name with timestamp
     */
    public static function getLatestPublic($file_path)
    {
        if (is_file(public_path().$file_path)) {
            return $file_path.'?'.filemtime(public_path().$file_path);
        } else {
            return $file_path.'?'.time();
        }
    }

    /**
     * Create a folder and set its permissions
     *
     * @param string $path Absolute path to the folder to create
     * @param int $chmod Octal number of permissions
     * @return boolean True if succeed
     */
    public function createPath($path, $chmod = self::CHMOD)
    {
        $this->path = false;
        if (!file_exists($path)) {
            if (!(new FolderService)->setPath(dirname($path)) || !touch($path)) {
                return false;
            }
        }
        return $this->setPath($path) && $this->setPermissions($chmod);
    }

    /**
     * Create ZIP of current file path
     *
     * @param string $zip_path Absolute path of the zip to create
     * @return boolean True if succeed
     */
    public function createZip($zip_path)
    {
        $zip = new \ZipArchive();
        if ($this->path && is_file($this->path)) {
            // Remove old ZIP file
            if (is_file($zip_path)) {
                @unlink($zip_path);
            }
            if ($zip->open($zip_path, \ZipArchive::CREATE) === true) {
                $file = new \SplFileInfo($this->path);
                $basename = $file->getFilename();
                $zip->addFile($this->path, $basename);
                return $zip->close();
            }
        }
        return false;
    }
}
