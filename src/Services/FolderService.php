<?php

namespace Brunoocto\Filesystem\Services;

use Brunoocto\Filesystem\Contracts\FolderInterface;
use Brunoocto\Filesystem\Abstracts\FilesystemAbstract;

/**
 * Handle Folder operations
 *
 */
class FolderService extends FilesystemAbstract implements FolderInterface
{
    /**
     * Return a list of all Files and Folders of the current path
     *
     * @return array Array of files and folders presents in current path
     */
    public function loopFolder()
    {
        $list = [];
        if ($this->path !== false) {
            if ($this->checkPath($this->path)) {
                // Get all full path of files and folder without "." and ".."
                $dirname = static::realpath($this->path);
                $files = array_map(function ($file) use ($dirname) {
                    return $dirname.'/'.$file;
                }, array_diff(scandir($dirname), array('..', '.')));
                if (is_array($files) && count($files) > 0) {
                    foreach ($files as $file) {
                        $prefix = 'other';
                        if (is_file($file)) {
                            $prefix = 'file';
                        } elseif (is_dir($file)) {
                            $prefix = 'folder';
                        }
                        $suffix = is_link($file) ? '_link' : '';
                        $list[$prefix.$suffix][basename($file)] = static::realpath($file);
                    }
                }
            }
        }
        return $list;
    }

    /**
     * Create a folder and set its permissions
     *
     * @param string $path Absolute path to the folder to create
     * @param int $chmod Octal number of permissions
     * @return boolean True if succeed
     */
    public function createPath($path, $chmod = self::CHMOD)
    {
        $this->path = false;
        if (!file_exists($path)) {
            if (!mkdir($path, $chmod, true) || !$this->setPath($path)) {
                return false;
            }
        }
        return $this->setPath($path) && $this->setPermissions($chmod);
    }

    /**
     * Create ZIP of current folder path
     *
     * @param string $zip_path Absolute path of the zip to create
     * @return boolean True if succeed
     */
    public function createZip($zip_path)
    {
        $zip = new \ZipArchive();
        if ($this->path && is_dir($this->path)) {
            // Remove old ZIP file
            if (is_file($zip_path)) {
                @unlink($zip_path);
            }
            if ($zip->open($zip_path, \ZipArchive::CREATE) === true) {
                $filelist = new \RecursiveIteratorIterator(new \RecursiveDirectoryIterator($this->path), \RecursiveIteratorIterator::SELF_FIRST);
                foreach ($filelist as $key => $file) {
                    $basename = $file->getFilename();
                    if (!in_array($basename, array('.', '..'))) {
                        if (is_dir($key)) {
                            $zip->addEmptyDir($basename);
                        } elseif (is_file($key)) {
                            $zip->addFile($key, $basename);
                        }
                    }
                }
                return $zip->close();
            }
        }
        return false;
    }
}
