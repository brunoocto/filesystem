<?php

namespace Brunoocto\Filesystem\Abstracts;

use Brunoocto\Filesystem\Contracts\FilesystemInterface;

/**
 * Handle Filesystem operations
 *
 */
abstract class FilesystemAbstract implements FilesystemInterface
{
    /**
     * Permissions in octal value
     *
     * @var integer
     */
    protected const CHMOD = 0750;

    /**
     * Path of the filesystem we are working with
     *
     * @var string|false Absolute path to the filesystem
     */
    protected $path = false;

    /**
     * Return the real path of a file or folder
     *
     * @param string $path absolute path of the filesystem
     * @return string
     */
    public static function realpath($path)
    {
        // If the original function work, we return the result
        if (\realpath($path)) {
            return \realpath($path);
        // If not, we make sure we delete the last "/" to display as it should be from the original function
        } elseif (substr($path, -1) == '/') {
            $path = substr($path, 0, -1);
        }
        return $path;
    }

    /**
     * Check if the path exists
     *
     * @param string $path absolute path of the filesystem
     * @return boolean True if the path exists
     */
    protected function checkPath($path)
    {
        return file_exists($path);
    }

    /**
     * Constructor
     *
     * @param string|false Absolute path to the filesystem
     * @param boolean $create At True it forces to create the path
     * @return void
     */
    public function __construct($path = false, $create = false)
    {
        if ($path) {
            $this->setPath($path, $create);
        }
    }

    /**
     * Returns whether the file path is an absolute path.
     * https://github.com/symfony/filesystem
     *
     * @return bool
     */
    public function isAbsolutePath(string $path)
    {
        return strspn($path, '/\\', 0, 1)
            || (
                \strlen($path) > 3 && ctype_alpha($path[0])
                && ':' === $path[1]
                && strspn($path, '/\\', 2, 1)
            )
            || null !== parse_url($path, PHP_URL_SCHEME)
        ;
    }

    /**
     * Set the path used by the instance
     *
     * @param string $path absolute path from filesystem
     * @param boolean $create At True it forces to create the path
     * @return boolean True if succeed
     */
    public function setPath($path, $create = false)
    {
        $this->path = false;
        // Reject if the path is relative
        if (!$this->isAbsolutePath($path)) {
            trigger_error('The path is not absolute "'.$path.'"', E_USER_WARNING);
        } elseif ($this->checkPath($path)) {
            $this->path = static::realpath($this->correctPathString($path));
            return true;
        } elseif ($create) {
            if ($this->createPath($path)) {
                $this->path = static::realpath($this->correctPathString($path));
                return true;
            }
        }
        return false;
    }

    /**
     * Get filesystem path
     *
     * @return string Absolute path
     */
    public function getPath()
    {
        return static::realpath($this->path);
    }

    /**
     * If the filesystem exists, set its permissions
     *
     * @param int $chmod Octal number of permissions
     * @return boolean True if succeed
     */
    public function setPermissions($chmod = self::CHMOD)
    {
        if ($this->path !== false) {
            if ($this->checkPath($this->path)) {
                return chmod($this->path, $chmod);
            }
        }
        return false;
    }

    /**
     * Create a symlink
     *
     * @param string $symlink Absolute path of the symlink to create
     * @return boolean True if succeed
     */
    public function createSymlink($symlink)
    {
        // Create the symlink
        if ($this->path && !file_exists($symlink)) {
            // Unlink is used is case the root path is different (for example IDE docker and PHP docker doesn 't have the same root path), it avoid an error because it may actuali exists when if file_exists return false.
            @unlink($symlink);
            if (symlink($this->path, $symlink)) {
                return $this->setPath($symlink);
            }
        }
        return false;
    }

    /**
     * Recursively remove a Directory or simple remove a file
     *
     * @param false|string $path relative or absolute path of the filesystem
     * @return boolean At True it successfully remove an existing folder
     */
    public function recursiveRemove($path = false)
    {
        // Take instance path by default
        if ($path == false) {
            $path = $this->path;
        }
        $result = false;
        if ($this->checkPath($path)) {
            if (is_dir($path) && !is_link($path)) {
                // Get all full path of files and folder without "." and ".."
                $dirname = static::realpath($path);
                $files = array_map(function ($file) use ($dirname) {
                    return $dirname.'/'.$file;
                }, array_diff(scandir($dirname), array('..', '.')));
                // Recursive loop if it's a real folder
                foreach ($files as $file) {
                    if (is_dir($file) && !is_link($file)) {
                        $this->recursiveRemove($file);
                    } else {
                        @unlink($file);
                    }
                }
                $result = @rmdir($path);
            } else {
                $result = @unlink($path);
            }
        }
        return $result;
    }

    /**
     * Add last slash for folder if it's missing
     *
     * @param string $path absolute path from filesystem
     * @return string Corrected path string
     */
    protected function correctPathString(string $path)
    {
        if ($this->checkPath($path) && is_dir($path)) {
            if (substr($path, -1) != '/') {
                $path = $path.'/';
            }
        }
        return $path;
    }

    /**
     * Create a filesystem and set its permissions
     *
     * @param string $path Absolute path to the filesystem to create
     * @param int $chmod Octal number of permissions
     * @return boolean True if succeed
     */
    abstract public function createPath($path, $chmod = self::CHMOD);

    /**
     * Create ZIP of current folder path
     *
     * @param string $zip_path Absolute path of the zip to create
     * @return boolean True if succeed
     */
    abstract public function createZip($zip_path);
}
