<?php

namespace Brunoocto\Filesystem\Providers;

use Dotenv\Dotenv;
use Illuminate\Support\ServiceProvider;

/*
 * This importation specify the real service used.
 */
use Brunoocto\Filesystem\Services\FileService;
use Brunoocto\Filesystem\Contracts\FileInterface;
use Brunoocto\Filesystem\Services\FolderService;
use Brunoocto\Filesystem\Contracts\FolderInterface;

class FilesystemServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        // Binding
        // The singleton (or bind) set any specification at instanciation
        $this->app->bind(FileInterface::class, FileService::class);
        $this->app->bind(FileService::class, function () {
            return new FileService;
        });
        $this->app->bind(FolderInterface::class, FolderService::class);
        $this->app->bind(FolderService::class, function () {
            return new FolderService;
        });
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        // Load environment variables specific to the library (default is .env)
        $dotenv = Dotenv::createMutable(__DIR__.'/../../');
        $dotenv->load();
    }
}
