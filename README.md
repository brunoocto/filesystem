# Introduction<br>
<br>
Handle File and Directory operations<br>
<br>
<br>
## Documentation<br>
<br>
Please refer to the online wiki:<br>
[wiki](https://gitlab.com/brunoocto/filesystem/wikis/home)
<br>
<br>
## Installation<br>
<br>
In your framework root directory, open composer.json and add the following repository:<br>
```json
    [...]
    "repositories": [
        [...]
        {
            "type": "vcs",
            "url":  "git@gitlab.com:brunoocto/filesystem.git"
        }
        [...]
    ]
    [...]
```
<br>
<br>
Then import the library via composer:<br>
```console
me@dev:# composer require brunoocto/filesystem:"~1.0"
```
<br>
<br>
## Tests<br>
<br>
Run all Feature tests, Unit tests, and generate a Coverage report:<br>
```console
me@dev:# phpunit
```
<br>
